<?php
//while loop
$i = 1 ;
while(1)

{
  if($i>9 )  {
      break;
  }
    echo $i . "";
    $i = $i + 2;
}

// end of while loop

echo "<br>";
echo "php - ";


$i= 1;

while ($i<=44)
{
    echo "B$i";
    if($i<44){
        echo ", ";
    }
    else{
        echo ".";
    }

    $i++;

}
echo "<br>";
//do while loop
$i=5;
do{
    echo "hello $i <br>";
    $i++;

}while($i<=10);

// example1
echo "<br>";
$i=11;
do{
    echo "hello $i <br>";
    $i++;

}while($i<=10);
// example2
echo "<br>";
$i=12;
do{
    echo "hello $i <br>";
    $i++;

}while($i<=10);

//for loop
echo "<br>";

for($i=1; $i>10; $i++){

    echo"hi $i";
}
//example 1, two dymentional  arry
echo "<br>";
$myArray = array(array());
$count = 1;
for($i = 0; $i<=3;$i++){
    for($j=0;$j<=3;$j++){
        $myArray[$i][$j] = $count;
        $count++;

    }
}
echo "<pre>";
var_dump ($myArray);
echo "</pre>";

// show in table
echo "<br>";
$myArray = array(array());
$count = 1;
echo "<table border= '2'>";

for($i = 0; $i<=3;$i++){
    echo "<tr>";
    for($j=0;$j<=3;$j++){
        $myArray[$i][$j] = $count;
        echo "<td>" ;
        echo ($myArray[$i][$j]);
        echo "</td>";

        $count++;

    }
    echo"</tr>";
}
echo "</table>";

//exercise
echo "<br>";
$myArray = array(array());
$count = 1;
echo "<table border= '2'>";

for($i =0; $i<=3; $i++){
    echo "<tr>";
    for($j=0;$j<=$i;$j++){
        $myArray[$i][$j] = $count;
        echo "<td>" ;
        echo ($myArray[$i][$j]);
        echo "</td>";

        $count++;

    }
    echo"</tr>";
}
echo "</table>";
echo "<br>";
//for each loop
$ageArray = array ("Abul"=>32, "kadija"=>12,"Moyna"=>50);
foreach($ageArray as $key=>$value){
    echo "ageArray['$key'] is $value<br>";

}
